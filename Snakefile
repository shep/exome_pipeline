configfile: "config.yaml"
import pandas as pd
import os
from pathlib import Path

# Metadata
sam=pd.read_csv("seqdata.csv")

ACCS=["SRR2962692", "SRR2962694", "SRR2962669", "SRR2962693"]
sam=sam.loc[sam["Run"].isin(ACCS)]
sam=sam.set_index("Sample Name")
SAMPLES=list(sam.index)

CHROMS=[ "chr" + str(i) for i in range(1,22) ]
CHROMS.extend(["chr" + i for i in ["X", "Y"]])

PARTS=list(range(1,config["num_splits"] + 1))
#PARTS=list(range(1,10))

# Directories
Path("./raw").mkdir(exist_ok=True)
Path("./.logs").mkdir(exist_ok=True)
Path("./benchmarks").mkdir(exist_ok=True)


# Functions
def get_readgroup(wildcards):
    return str(repr("\t".join(["@RG", "ID:" + sam.at[os.path.basename(wildcards.seqname), "Run"], "PL:" + sam.at[os.path.basename(wildcards.seqname), "Platform"], "SM:" + os.path.basename(wildcards.seqname)])))


rule all:
    input:
        expand("final.{mode}.vcf.gz", mode=["SNP", "INDEL"])
        

rule get_sra:
    output:
        r1=protected("raw/{acc}_1.fastq.gz"),
        r2=protected("raw/{acc}_2.fastq.gz")
    shell:
        "fastq-dump --gzip --skip-technical --split-files {wildcards.acc} "
        "--outdir raw/"

# Trim adapters and filter low length sequences
rule seqtrim:
    input:
        r1=lambda wildcards: os.path.join("raw", sam.at[wildcards.sample, "Run"] + "_1.fastq.gz"),
        r2=lambda wildcards: os.path.join("raw", sam.at[wildcards.sample, "Run"] + "_2.fastq.gz")
    output:
        o1=temp("{sample}/{sample}_R1.trimmed.fastq.gz"),
        o2=temp("{sample}/{sample}_R2.trimmed.fastq.gz"),
        rep_html="{sample}/{sample}.fastp.html",
        rep_json="{sample}/{sample}.fastp.json"
    threads:config["sample_threads"]
    params:
        min_length=config["min_read_length"]
    shell:
        "mkdir -p {wildcards.sample} && "
        "fastp -i {input.r1} -I {input.r2} "
        "-o {output.o1} -O {output.o2} "
        "--detect_adapter_for_pe "
        "--thread {threads} "
        "--length_required {params.min_length} "
        "--json {output.rep_json} "
        "--html {output.rep_html}"


# Align sequence to a reference genome
rule alignment:
    input:
        r1="{seqname}_R1.trimmed.fastq.gz",
        r2="{seqname}_R2.trimmed.fastq.gz"
    output:
        temp("{seqname}.unsorted.bam")
    threads:config["sample_threads"]
    params:
        idx=config["bwa_index"],
        arch=config["arch"],
        readgroup=get_readgroup
    shell:
        "bwa-mem2.{params.arch} mem {params.idx} {input.r1} {input.r2} "
        "-R {params.readgroup} "
        "-t {threads} | "
        "samtools view -Sb - "
        "> {output}"


# Sort alignment files by coordinate
rule alignment_sort:
    input:
        "{seqname}.unsorted.bam"
    output:
        sb=temp("{seqname}.sorted.bam"),
        idx=temp("{seqname}.sorted.bam.bai")
    threads:config["sample_threads"]
    resources: mem_mb=config["sample_mem_mb"]
    shell:
        "samtools sort {input} "
        "-m {resources.mem_mb}M "
        "-@ {threads} "
        "-o {output.sb} && "
        "samtools "
        "index {output.sb} "
        "-@ {threads} -b"


# Sort alignment files by queryname
rule alignment_readname_sort:
    input:
        "{seqname}.unsorted.bam"
    output:
        sb=temp("{seqname}.readname_sorted.bam")
    threads:config["sample_threads"]
    resources: mem_mb=config["sample_mem_mb"]
    shell:
        "samtools sort -n {input} "
        "-m {resources.mem_mb}M "
        "-@ {threads} "
        "-o {output.sb}"


# Mark duplicates
rule mark_duplicates:
    input:
        "{seqname}.readname_sorted.bam"
    output:
        bd=temp("{seqname}.dedup.bam"),
        blog="{seqname}.dedup_metrics.txt",
        bii=temp("{seqname}.dedup.bam.bai"),
        sbi=temp("{seqname}.dedup.bam.sbi")
    shell:
        "/groups/nshomron/guyshapira/bin/gatk "
        "MarkDuplicatesSpark "
        "--spark-runner LOCAL "
        "-I {input} "
        "-M {output.blog} "
        "-O {output.bd}"


# Base quality score recalibration
rule calc_BQSR:
    input:
        ib="{seqname}.dedup.bam",
        iib="{seqname}.dedup.bam.bai"
    output:
        "{seqname}.bsqr.txt"
    params:
        ref=config["genome_ref"],
        dbsnp=config["dbsnp"],
        indels1=config["known_indels"],
        indels2=config["mills_indels"]
    resources: mem_mb=config["sample_mem_mb"]
    shell:
        "/groups/nshomron/guyshapira/bin/gatk "
        "--java-options \"-Dsamjdk.use_async_io_read_samtools=true -Dsamjdk.use_async_io_write_samtools=true -Dsamjdk.use_async_io_write_tribble=true "
        "-Xmx{resources.mem_mb}m -XX:ParallelGCThreads=4\" BaseRecalibrator "
        "-I {input.ib} -R {params.ref} "
        "--known-sites {params.dbsnp} "
        "--known-sites {params.indels1} "
        "--known-sites {params.indels2} "
        "-O {output}"


# Apply base quality score recalibration
rule apply_BQSR:
    input:
        ib="{seqname}.dedup.bam",
        iib="{seqname}.dedup.bam.bai",
        bb="{seqname}.bsqr.txt"
    output:
        ob="{seqname}.recalibrated.bam",
        ib="{seqname}.recalibrated.bai"
    params:
        ref=config["genome_ref"]
    resources: mem_mb=config["sample_mem_mb"]
    threads:8
    shell:
        "/groups/nshomron/guyshapira/bin/gatk --java-options \"-Xmx{resources.mem_mb}m -XX:ParallelGCThreads=4\" ApplyBQSR "
        "-I {input.ib} -R {params.ref} "
        "--bqsr-recal-file {input.bb} "
        "-O {output.ob}"


# Intermediate variant calling
rule haplotype_caller:
    input:
        ib="{sample}/{sample}.recalibrated.bam",
        iib="{sample}/{sample}.recalibrated.bai"
    output:
        ob=temp("{sample}/{sample}_cont{part}.g.vcf.gz"),
        obi=temp("{sample}/{sample}_cont{part}.g.vcf.gz.tbi")
    params:
        ref=config["genome_ref"],
        intervals=config["ranges"],
        pad=config["interval_padding"],
        splt=config["split_path"]
    resources: mem_mb=25000
    benchmark:"benchmarks/{sample}.haplotype_caller_cont{part}.tsv"
    threads:12
    shell:
        "/groups/nshomron/guyshapira/bin/gatk --java-options \"-Xmx{resources.mem_mb}m -XX:ParallelGCThreads=2\" HaplotypeCaller "
        "-I {input.ib} -R {params.ref} -O {output.ob} "
        "--native-pair-hmm-threads {threads} "
        "--intervals {wildcards.part} "
        "--interval-padding {params.pad} "
        "-ERC GVCF"


# Consolidate interval GVCFs
rule gather_gvcfs:
    input:
        ib=lambda x: [ "{0}/{0}_cont{1}.g.vcf.gz".format(x.sample, str(pp)) for pp in CHROMS ],
        ibi=lambda x: [ "{0}/{0}_cont{1}.g.vcf.gz.tbi".format(x.sample, str(pp)) for pp in CHROMS ]
    output:
        ob=temp("{sample}/{sample}_merged.g.vcf.gz"),
        iib=temp("{sample}/{sample}_merged.g.vcf.gz.tbi")
    params:
        gvcfs=lambda x: " -I ".join([ "{0}/{0}_cont{1}.g.vcf.gz".format(x.sample, str(pp)) for pp in CHROMS ])
    resources:
        mem_mb=config["sample_mem_mb"]
    shell:
        "/groups/nshomron/guyshapira/bin/gatk --java-options \"-Xmx{resources.mem_mb}m\" GatherVcfs "
        "-I {params.gvcfs} "
        "-O {output.ob} && "
        "bcftools index --tbi "
        "--threads {threads} "
        "{output.ob}"


rule genomics_db_import:
    input:
        gvcfs=expand("{sample}/{sample}_merged.g.vcf.gz", sample=SAMPLES),
        gvcfsi=expand("{sample}/{sample}_merged.g.vcf.gz.tbi", sample=SAMPLES)
    output:
        db=directory("genomicdb"),
        check=os.path.join("genomicdb", ".done")
    params:
        pad=config["interval_padding"],
        intervals=config["ranges"],
        gvcfs=" -V ".join([ os.path.join(s, s + "_merged.g.vcf.gz") for s in SAMPLES ])
    resources:
        mem_mb="250000"
    shell:
        "/groups/nshomron/guyshapira/bin/gatk --java-options \"-Xmx50g -Xms50g\" GenomicsDBImport "
        "--genomicsdb-workspace-path genomicdb/ "
        "--overwrite-existing-genomicsdb-workspace"
        " -V {params.gvcfs} "
        "-L {params.intervals} --interval-padding {params.pad} --merge-input-intervals "
        "--max-num-intervals-to-import-in-parallel 20 "
        "--tmp-dir /data/guyshapira/tmp/ --genomicsdb-segment-size 10485760 "
        "--genomicsdb-shared-posixfs-optimizations --genomicsdb-vcf-buffer-size 163840 "
        " && touch {output.check} "


rule GenotypeGVCFs:
    input:
        os.path.join("genomicdb", ".done")
    output:
        vcf=temp("variants_cont{part}.vcf.gz"),
        tbi=temp("variants_cont{part}.vcf.gz.tbi")
    resources:mem_mb=2500
    params:
        tmp=config["tmp_large"],
        ref=config["genome_ref"],
        splt=config["split_path"]
    threads:4
    shell:
        "/powerapps/share/centos7/miniconda/miniconda3-4.7.12-environmentally/envs/GuyShapira/bin/gatk "
        "--java-options \"-Xmx{resources.mem_mb}m -XX:ParallelGCThreads=4\" GenotypeGVCFs "
        "-V gendb://genomicdb/ "
        "--genomicsdb-shared-posixfs-optimizations true "
        "--intervals {wildcards.part} "
        "--tmp-dir {params.tmp} -R {params.ref} "
        "-G StandardAnnotation -G AS_StandardAnnotation "
        "-O {output.vcf}"
 

rule ConcatVCFs:
    input:
        vcfs=expand("variants_cont{part}.vcf.gz", part=CHROMS)
    output:
        vcf="merged.vcf.gz",
        tbi="merged.vcf.gz.tbi"
    resources: mem_mb=config["sample_mem_mb"]
    threads:config["sample_threads"]
    params:
        vcfs=" ".join(["variants_cont{0}.vcf.gz".format(p) for p in CHROMS ])
    shell:
        "bcftools concat "
        "--output {output.vcf} --output-type z "
        "--threads {threads} "
        "{params.vcfs} && "
        "bcftools index --tbi "
        "--threads {threads} "
        "{output.vcf}"



rule recalibrate_snps:
    input:
        ib="merged.vcf.gz",
        iib="merged.vcf.gz.tbi"
    output:
        ob="output.SNP.recal",
        iob="output.SNP.recal.idx",
        tranches="tranches.SNP.out",
        rout="plots.SNP.R",
        rpdf="plots.SNP.R.pdf"
    params:
        ref=config["genome_ref"],
        max_gaussians=3
    resources: mem_mb=30000
    threads:4
    shell:
        "/groups/nshomron/guyshapira/bin/gatk --java-options \"-XX:+UseParallelGC -XX:ParallelGCThreads=4 -Xmx10000m\" VariantRecalibrator "
        "-mode SNP -AS "
        "-V {input.ib} -R {params.ref} "
        "-O {output.ob} --tranches-file {output.tranches} --rscript-file {output.rout} "
        "-tranche 100.0 -tranche 99.95 -tranche 99.9 -tranche 99.8 -tranche 99.6 -tranche 99.5 -tranche 99.4 -tranche 99.3 -tranche 99.0 -tranche 98.0 -tranche 97.0 -tranche 90.0 "
        "-an QD -an MQRankSum -an ReadPosRankSum -an FS -an MQ -an SOR -an DP --max-gaussians {params.max_gaussians} --trust-all-polymorphic "
        "--resource:hapmap,known=false,training=true,truth=true,prior=15 /groups/nshomron/guyshapira/tmp/hapmap_3.3.hg38.vcf.gz "
        "--resource:omni,known=false,training=true,truth=false,prior=12 /groups/nshomron/guyshapira/tmp/omni/1000G_omni2.5.hg38.vcf.gz "
        "--resource:1000G,known=false,training=true,truth=false,prior=10 /groups/nshomron/guyshapira/tmp/1000G_phase1.snps.high_confidence.hg38.vcf.gz "
        "--resource:dbsnp,known=true,training=false,truth=false,prior=5 /groups/nshomron/guyshapira/tmp/dbsnp/Homo_sapiens_assembly38.dbsnp138.vcf.gz"


rule recalibrate_indels:
    input:
        ib="merged.vcf.gz",
        iib="merged.vcf.gz.tbi"
    output:
        ob="output.INDEL.recal",
        iob="output.INDEL.recal.idx",
        tranches="tranches.INDEL.out",
        rout="plots.INDEL.R",
        rpdf="plots.INDEL.R.pdf"
    params:
        ref=config["genome_ref"],
        max_gaussians=3
    resources: mem_mb=30000
    threads:4
    shell:
        "/groups/nshomron/guyshapira/bin/gatk --java-options \"-XX:+UseParallelGC -XX:ParallelGCThreads=4 -Xmx10000m\" VariantRecalibrator "
        "-mode INDEL -AS "
        "-V {input.ib} -R {params.ref} "
        "-O {output.ob} --tranches-file {output.tranches} --rscript-file {output.rout} "
        "-tranche 100.0 -tranche 99.95 -tranche 99.9 -tranche 99.8 -tranche 99.6 -tranche 99.5 -tranche 99.4 -tranche 99.3 -tranche 99.0 -tranche 98.0 -tranche 97.0 -tranche 90.0 "
        "-an QD -an MQRankSum -an ReadPosRankSum -an FS -an MQ -an SOR -an DP --max-gaussians {params.max_gaussians} --trust-all-polymorphic  "
        "--resource:mills,known=false,training=true,truth=true,prior=12.0 /groups/nshomron/guyshapira/tmp/Mills_and_1000G_gold_standard.indels.hg38.vcf.gz "
        "--resource:dbsnp,known=true,training=false,truth=false,prior=2.0 /groups/nshomron/guyshapira/tmp/dbsnp/Homo_sapiens_assembly38.dbsnp138.vcf.gz "


rule apply_calibration:
    input:
        ib="merged.vcf.gz",
        iib="merged.vcf.gz.tbi",
        recal="output.{mode}.recal",
        tranches="tranches.{mode}.out"
    output:
        oo=temp("recalibrated.{mode}.vcf.gz"),
        oi=temp("recalibrated.{mode}.vcf.gz.tbi")
    params:
        ref=config["genome_ref"]
    resources: mem_mb=50000
    threads:4
    shell:
        "/groups/nshomron/guyshapira/bin/gatk --java-options \"-XX:+UseParallelGC -XX:ParallelGCThreads=4 -Xmx{resources.mem_mb}m\" "
        "ApplyVQSR -AS -V {input.ib} --recal-file {input.recal} -O {output.oo} "
        "-mode {wildcards.mode} "
        "--tranches-file {input.tranches}"


rule calc_posteriors:
    input:
        ib="recalibrated.{mode}.vcf.gz",
        iib="recalibrated.{mode}.vcf.gz.tbi"
    output:
        ob="final.{mode}.vcf.gz",
        obo="final.{mode}.vcf.gz.tbi"
    params:
        ref=config["genome_ref"],
        ped=config["pedigree"]
    resources: mem_mb=50000
    threads:4
    shell:
        "/groups/nshomron/guyshapira/bin/gatk --java-options \"-Xmx{resources.mem_mb}m\" CalculateGenotypePosteriors "
        "-V {input.ib} --skip-population-priors -ped {params.ped} -O {output.ob}"
