# Deprecated!
Use the dnaseq pipeline instead

# DNASeq pipeline
An end-to-end pipeline for whole-exome variant calling and downstream analysis.
This is a Snakemake implementation of the GATK best practices, with performance
adjustments for SLURM execution. The goal of this pipeline is to take advantage
of the complete GATK toolset (Allele specific annotations, read-grouping,
etc.), while maintaining speed and reusability.

## Notes
 - The existing configurations demonstrate the pipeline by downloading a NIST
   reference trio and genotyping it thoroughly, including pedigree-based posterior
   calculation.
 - The workflow is configured via config.yaml and seqdata.csv.
 - GATK v4.2.4.1 (latest) is used for all operations, except for genotypeGVCFs,
   which uses v4.2.4.1, [due to a fatal bug](https://github.com/broadinstitute/gatk/issues/7639).
