# Get csv file and convert it to pedigree format (PED)
import pandas as pd
import numpy as np

sam=pd.read_csv("pedigree.ped.csv")

sam["sex"]=[ {"male":"1", "female":"2"}[l] if l in ["male", "female"] else "-9" for l in sam["sex"]]
sam["phenotype"]=[ {"affected":2, "unaffected":1} if l in ["affected", "unaffected"] else -9 for l in sam["phenotype"]]
sam["maternal_id"]=[ l if l is not np.nan else 0 for l in sam["maternal_id"]]
sam["paternal_id"]=[ l if l is not np.nan else 0 for l in sam["paternal_id"]]



sam[["family_id","individual_id","paternal_id","maternal_id","sex", "phenotype"]].to_csv("pedigree.ped", sep="\t", index=None, header=None)
